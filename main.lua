function car(l)
	-- lol, 'type saftey'
	return l[1]
end


function cdr(l)
	return l[2]
end


function tounit(n) -- tablerep(5) = {{}, {}, {}, {}, {}}
	local t = {}
	for i = 1, n do
		t[#t+1] = {}
	end
	return t
end


function breakstring(str)
	local t = {}
	for i = 1, #str do
		t[#t+1] = str:sub(i,i)
	end
	return t
end


function joinstring(t)
	local str = ""
	for i = 1, #t do
		str = str .. t[i]
	end
	return str
end


function Print(...)
	local args = {...}
	function _(t)
		if type(t) == "table" then
			io.write("{")
			for i = 1, #t do
				io.write(_(t[i]))
				if i < #t then
					io.write(", ")
				end
			end
			io.write("}")
		elseif type(t) == "boolean" then
			if t then io.write("true") else io.write("false") end
		elseif type(t) == "nil" then
			io.write("nil")
		else
			io.write(tostring(t))
		end
	end
	for i = 1,#args do
		_(args[i])
		if i < #args then io.write("\t") end
	end
	print("")
end


Type = {
	"null",
	"symbol",
	"number",
	"character",
	"list",
	"string",
	"luafunction",
}
TypeDispatch = {} -- filled in later

-- allow reverse lookup (aka. Type["character"]
for i, type in ipairs(Type) do
	Type[type] = i
end


function istype(list)
	local typelist = car(list)
	local i = #typelist

	if Type[i] then
		return Type[i]
	end
	return "unknown type: " .. i
end


-- lmao, this would actually be more efficient if lua was indexed by zero
null_list = {{{}}}


-- lol
function tonull(list)
	return nil
end

function tosymbol(list)
	if istype(list) ~= "symbol" then return nil end
	local n = #cdr(list)
	local s = indexsymbol(n)
	return "#" .. s.name
end


function tonumber(list)
	if istype(list) ~= "number" then return 0 end
	local n = #cdr(list)
	return n
end

function tocharacter(list)
	if istype(list) ~= "character" then return "" end
	local n = #cdr(list)
	return string.char(n)
end

function tolist(list)
	if istype(list) ~= "list" then return nil end
	local t = {}
	local valuelist = cdr(list)
	for i, lis in ipairs(valuelist) do
		t[i] = TypeDispatch[istype(lis)](lis)
	end
	return t
end

function _tostring(list)
	if istype(list) ~= "string" then return "" end
	local chartable = cdr(list)
	local t = {}
	for i = 1,#chartable do
		t[i] = tocharacter(chartable[i])
	end
	return joinstring(t)
end

function toluafunction(list)
	if istype(list) ~= "luafunction" then return nil end
	local valuelist = cdr(list)
	return valuelist
end

-- fill in the type dispatch table
TypeDispatch["null"] = tonull
TypeDispatch["symbol"] = tosymbol
TypeDispatch["number"] = tonumber
TypeDispatch["character"] = tocharacter
TypeDispatch["list"] = tolist
TypeDispatch["string"] = _tostring
TypeDispatch["luafunction"] = toluafunction

function tolua(list)
	return TypeDispatch[istype(list)](list)
end

function tohuman(list)
	local type = istype(list)
	if type == "null" then
		return {type,}
	elseif type == "symbol" then
		return {type, tosymbol(list)}
	elseif type == "character" then
		local c = tocharacter(list)
		return {type, c}
	elseif type == "number" or type == "luafunction" then
		return {type, list[2]}
	elseif type == "string" or type == "list" then
		local t = {}
		for i,v in ipairs(cdr(list)) do
			t[i] = tohuman(v)
		end
		return {type, t}
	end
	return {}
end

Symbol = {}

function newsymbol(name, value)
	local i = #Symbol+1
	local symbolitem = {
		i = i,
		name = name,
		value = value,
	}

	Symbol[i] = symbolitem
	Symbol[name] = symbolitem
end

function indexsymbol(name)
	local value = (type(name) == "string" and name:sub(1,1) == "#") and name:sub(2,-1) or name
	if not Symbol[value] then
		newsymbol(value, null_list)
	end
	return Symbol[value]
end

function internalnull(value)
	return {tounit(Type["null"])}
end

function internalsymbol(nameorindex)
	local index = indexsymbol(nameorindex)
	return {tounit(Type["symbol"]), tounit(index.i)}
end

function internalnumber(number)
	return {tounit(Type["number"]), tounit(number)}
end

function internalcharacter(character)
	return {tounit(Type["character"]), tounit(string.byte(character))}
end

function internallist(values)
	return {tounit(Type["list"]), values}
end

function internalstring(str)
	local charlist = breakstring(str)
	for k,v in ipairs(charlist) do
		charlist[k] = internalcharacter(v)
	end
	return {tounit(Type["string"]), charlist}
end

function internalluafunction(fun)
	return {tounit(Type["luafunction"]), fun}
end

function internalconvert(value)	
	if type(value) == "string" then
		if #value > 0 and "#" == value:sub(1,1) then
			return internalsymbol(value:sub(2,-1))
		end
		return internalstring(value)
	elseif type(value) == "number" then
		return internalnumber(value)
	elseif type(value) == "nil" then
		return null_list
	elseif type(value) == "table" then
		local t = {}
		for i = 1,#value do
			t[i] = internalconvert(value[i])
		end
		return internallist(t)
	elseif type(value) == "function" then
		return internalluafunction(value)
	end
	print("I dunno what to do here")
	return null_list
end


function internalcar(list)
	if type(list) ~= "table" or type(list[2]) ~= "table" then return null_list end
	local value = cdr(list)
	return value[1] or null_list
end

function internalcdr(list)
	if type(list) ~= "table" or type(list[2]) ~= "table" then return null_list end
	local value = cdr(list)
	local t = {}
	for i=2,#value do
		t[i-1] = value[i]
	end
	return internallist(t)
end

-- this is just a dummy, really
newsymbol("quote", internalluafunction(function (x) return x end))

newsymbol("+", internalluafunction(function (env, ...)
	local n = 0;
	for i,v in ipairs({...}) do
		n = n + tonumber(v)
	end
	return internalnumber(n)
end))

function isquote(list)
	return istype(list) == "symbol" and tosymbol(list) == "#quote"
end


function islambda(list)
	return istype(list) == "symbol" and tosymbol(list) == "#lambda"
end


function lambdatoluafunction(list)
	-- list: {nil | {#sym, ...}, {...}}
	if not list or not list[2] or not list[2][1] or not list[2][2] or 
	   istype(list[2][1]) ~= "list" or istype(list[2][2]) ~= "list" then
	   	return internalconvert({"#error", "bad lambda"})
	end

	return internalluafunction(function (...)
		local args = {...}
		local env = table.remove(args, 1)

		local lambda = list 
		local larglist = cdr(car(cdr(lambda)))
		local body = cdr(cdr(lambda))

		for i,sym in pairs(larglist) do
			local pv = indexsymbol(#cdr(sym))
			local nv = {i = pv.i, name = pv.name, value = args[i]}
			env[pv.name], env[pv.i] = nv, nv
		end
		return eval(body, env)
	end)
end


function eval(list, env)
	env = env or Symbol
	local type = istype(list)
	if type == "list" then
		return apply(list, env)
	elseif type == "symbol" then
		local n = #cdr(list)
		local sym = env[n]
		return sym.value
	else
		return list
	end
end


function apply(list, env)
	local first = internalcar(list)
	local rest = internalcdr(list)

	if isquote(first) then
		local r = cdr(cdr(list))
		return r or null_list
	elseif islambda(first) then
		return lambdatoluafunction(rest)
	end

	first = eval(first)
	if istype(first) == "luafunction" then
		local args = cdr(rest)
		local t = {}
		t[1] = env
		for i = 1, #args do
			t[i+1] = eval(args[i], env)
		end
		return first[2](table.unpack(t))
	else
		return internalconvert({"#error", "bad function"})
	end
end


function test_number()
	local starty = 10
	local x = internalconvert(starty)
	local newy = tonumber(x)
	Print("test_number.0: ", type(starty) == type(newy))
	Print("test_number.1: ", starty == newy)
end

function test_string()
	local startstr = "abcdef"
	local x = internalconvert(startstr)
	local newstr = _tostring(x)
	Print("test_string.0: ", type(startstr) == type(newstr))
	Print("test_string.1: ", startstr == newstr)
end

function test_null()
	local startnull = nil
	local x = internalconvert(startnull)
	local newnull = tonull(x)
	Print("test_null.0: ", type(startnull) == type(newnull))
	Print("test_null.1: ", startnull == newnull)
end

function test_luafunction()
	local startfun = function (x) return x+1 end
	local x = internalconvert(startfun)
	local newfun = toluafunction(x)
	Print("test_function.0: ", type(startfun) == type(newfun))
	Print("test_function.1: ", startfun == newfun)
end

function test_symbol()
	local startsym = "#lispsymbol"
	local x = internalconvert(startsym)
	local endsym = tosymbol(x)
	Print("test_symbol.0: ", type(startsym) == type(endsym))
	Print("test_symbol.1: ", startsym == endsym)
end

function test_list()
	local startlist = {"abcd", 2}
	local x = internalconvert(startlist)
	local endlist = tolist(x)
	Print("test_list.0: ", type(endlist) == type(startlist))
	Print("test_list.1: ", type(endlist[1]) == type(startlist[1]))
	Print("test_list.2: ", endlist[1] == startlist[1])
	Print("test_list.3: ", type(endlist[2]) == type(startlist[2]))
	Print("test_list.4: ", endlist[2] == startlist[2])
end

function test_internalcdr() -- it's the More Complex Operation™
	local v = internalconvert({1,{1,4,5},3,4,5})
	local d = internalcdr(v)
	-- tests reproducability and lack of mutability
	Print("test_internalcdr.0: ", tolist(d)[1][1] == tolist(d)[1][1])
	Print("test_internalcdr.1: ", tolist(d)[2] == tolist(d)[2])
end

function test_nullquote()
	local t = {"#quote"}
	local x = internalconvert(t)
	local y = eval(x)
	Print("test_nullquote.0: ", type(y) == type(null_list))
	Print("test_nullquote.1: ", y == null_list)
end

function test_quote()
	local t = {"#quote", "#symbol"}
	local x = internalconvert(t)
	local y = eval(x)
	local z = tosymbol(y)
	Print("test_quote.0: ", type(z) == type(t[2]))
	Print("test_quote.1: ", z == t[2])
end

function test_apply()
	local t = {"#+", 2, 2}
	local r = 4
	local x = internalconvert(t)
	local y = eval(x)
	local z = tonumber(y)
	Print("test_apply.0: ", type(z) == type(r))
	Print("test_apply.1: ", z == r)
end

function test_nested_apply()
	local t = {"#+", {"#+", 2, 2}, 2}
	local r = 6
	local x = internalconvert(t)
	local y = eval(x)
	local z = tonumber(y)
	Print("test_nested_apply.0: ", type(z) == type(r))
	Print("test_nested_apply.1: ", z == r)
end

function test_lambda()
	local t = {{"#lambda", {"#x"}, {"#+", {"#+", 2, 2}, "#x"}}, 2}
	local r = 6
	local x = internalconvert(t)
	local y = eval(x)
	local z = tonumber(y)
	Print("test_lambda.0: ", type(z) == type(r))
	Print("test_lambda.1: ", z == r)
end

test_number()
test_string()
test_null()
test_luafunction()
test_symbol()
test_list()
test_internalcdr()
test_nullquote()
test_quote()
test_apply()
test_nested_apply()
test_lambda()

function ep(v) -- eval, print. given, say: {"#+", 2, 2} will show execution and result
	local x = internalconvert(v)
	Print("converted to:  ", x)
	Print("(human form):  ", tohuman(x))
	local y = eval(x)
	Print("result is:     ", y)
	Print("(human form):  ", tohuman(y))
	local z = tolua(y)
	Print("normalized to: ", z)
	return z
end
